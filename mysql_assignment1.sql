/*1.Create database using query*/

CREATE DATABASE mysql_assignment;
 

use mysql_assignment;
/*2.Create table using query (create 2 table members and moviews)*/
create table members
	(membership_number int primary key,
		full_names varchar(20),
		gender varchar(10),
		date_of_birth date, 
		physical_address varchar(50),
		postal_address varchar(50), 
		contact_number bigint,email varchar(50));

CREATE TABLE movies (
  movie_id int(11) primary key,
  title varchar(30) ,
  director varchar(20) ,
  year_released int(11) ,
  category_id int(11)
);

/* Create insert query   */
INSERT INTO members VALUES 
 ('1', 'Janet Jones', 'Female', '1980-07-21', 'First Street Plot No 4', 'Private Bag', '0759253542', 'janetjones@yagoo.cm'), 
 ('2', 'Janet Smit Jones', 'Female', '1989-06-23', 'Melrose 123', NULL, NULL, 'jj@fstreet.com'), 
 ('3', 'Robert Phill', 'Male', '1989-07-12', '3rd Street 34', NULL, '12345', 'rm@tstreet.com'), 
 ('4', 'Gloria Williams', 'Female', '14-02-14', '2nd Street 23', NULL, NULL, NULL);


 INSERT INTO movies VALUES 
 ('1', 'Pirates of the Caribean 4', 'Rob Marshall', '2011', '1'), 
 ('2', 'Forgetting Sarah Marshal', 'Nicholas Stoller', '2008', '2'), 
 ('3', 'X-Men', NULL, '2008', NULL), 
 ('4', 'Code Name Black', 'Edgar Jimz', '2010', NULL), 
 ('5', 'Daddy\'s Littel Girls', NULL, '2007', '8'), 
 ('6', 'Angles and Demons', NULL, '2007', '6'), 
 ('7', 'Davinci Code', NULL, '2007', '6'), 
 ('9', 'Honey mooners', 'John Schultz', '2005', '8'), 
 ('16', '67% Guilty', NULL, '2012', NULL);


/* 3.Create different-different Select query with DISTINCT,*,FROM, WHERE,GROUP BY,HAVING and HAVING*/
SELECT DISTINCT postal_address FROM members;


SELECT * FROM members;

SELECT * FROM movies WHERE year_released = 2007;

SELECT year_released FROM movies GROUP BY year_released;

SELECT * FROM movies GROUP BY year_released HAVING year_released=2008;


/* 4.Create query using WHERE Clause(IN, OR, AND, Not IN,Equal To,Not Equal To, Greater than, less than )*/

SELECT * FROM movies WHERE year_released  IN (2007,2008);

SELECT * FROM movies WHERE year_released  = 2007 OR director IS NULL;

SELECT * FROM movies WHERE year_released  = 2007 AND director IS NULL;

SELECT * FROM movies WHERE year_released  NOT IN (2007,2008);

SELECT * FROM movies WHERE year_released = 2008;

SELECT * FROM movies WHERE year_released != 2007;

SELECT * FROM movies WHERE year_released > 2008;

SELECT * FROM movies WHERE year_released < 2008;

/*6.Get record form members table using select query*/

SELECT * FROM members;

/*7.Getting only the full_names, gender, physical_address and email fields only from memeber table*/

SELECT full_names, gender, physical_address, email FROM members;

/*8.Get a member's personal details from members table given the membership number 1*/

SELECT * FROM members WHERE membership_number=1;

/*9.Get a list of all the movies in category 2 that were released in 2008*/

SELECT title FROM movies WHERE category_id=2 AND year_released=2008;

/*10.Gets all the movies in either category 1 or category 2*/

SELECT title FROM movies WHERE category_id IN (1,2);

/*11.Gets rows where membership_number is either 1 , 2 or 3*/

SELECT * FROM members WHERE membership_number IN (1,2,3);

/*12.Gets all the female members from the members table using the equal to comparison operator.*/

SELECT * FROM members WHERE gender = 'female';

/*13.Gets all the payments that are greater than 2,000 from the payments table*/

SELECT * FROM payments_tbl WHERE payment > 2000;



